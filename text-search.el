;;; text-search.el --- Library for searching text in buffers, files, URLs

;; Copyright © 2014 Alex Kost

;; Author: Alex Kost <alezost@gmail.com>
;; Created: 5 Nov 2014
;; URL: https://gitlab.com/alezost-emacs/text-search
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides some functions for unusual searching for strings
;; in buffers, files and URLs.

;;; Code:

(require 'cl-lib)

(defun search-buffer-for-matches (buffer regexp
                                  &optional count group reversep)
  "Find COUNT unique strings matching REGEXP in BUFFER.
Return a list of strings matching parenthesized GROUP
\(0 by default) in REGEXP.
If COUNT is nil, search the whole buffer (until search is failed).
If REVERSEP is non-nil, return a reverted list."
  (or count (setq count -1))
  (with-current-buffer buffer
    (save-excursion
      (goto-char (point-min))
      (let ((i 0)
            search-res search-list)
        (cl-loop do (setq search-res (re-search-forward regexp nil t))
                 until (or (= i count) (null search-res))
                 do
                 (let ((tmp search-list))
                   (cl-pushnew (match-string-no-properties (or group 0))
                               search-list
                               :test #'string=)
                   (or (eq tmp search-list)
                       (setq i (+ i 1)))))
        (if reversep
            search-list
          (nreverse search-list))))))

(defun search-url-for-matches (url regexp
                               &optional count group reversep)
  "Find COUNT unique strings matching regexp REGEXP in URL.
See `search-buffer-for-matches' for the meaning of COUNT, GROUP
and REVERSEP."
  (search-buffer-for-matches (url-retrieve-synchronously url)
                             regexp count group reversep))

(defun search-file-for-matches (file regexp
                                &optional count group reversep)
  "Find COUNT unique strings matching regexp REGEXP in FILE.
See `search-buffer-for-matches' for the meaning of COUNT, GROUP
and REVERSEP."
  (search-buffer-for-matches (find-file-noselect file t t)
                             regexp count group reversep))

(defun search-buffer-by-re-list (buffer regexps &optional group noerror)
  "Find string in BUFFER by consecutive searching for REGEXPS.
Return a string matching the parenthesized GROUP (0 by default)
in the last regexp from REGEXPS list.
If NOERROR is non-nil, just return nil if fail (no error)."
  (with-current-buffer buffer
    (save-excursion
      (goto-char (point-min))
      (let (result)
        (dolist (regexp regexps result)
          (setq result (re-search-forward regexp nil noerror)))
        (and result (match-string-no-properties (or group 0)))))))

(defun search-url-by-re-list (url regexps &optional group noerror)
  "Find string in BUFFER by consecutive searching for REGEXPS.
See `search-buffer-by-re-list' for the meaning of GROUP and NOERROR."
  (search-buffer-by-re-list (url-retrieve-synchronously url)
                            regexps group noerror))

(defun search-file-by-re-list (file regexps &optional group noerror)
  "Find string in BUFFER by consecutive searching for REGEXPS.
See `search-buffer-by-re-list' for the meaning of GROUP and NOERROR."
  (search-buffer-by-re-list (find-file-noselect file t t)
                            regexps group noerror))

(provide 'text-search)

;;; text-search.el ends here
